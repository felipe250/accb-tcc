<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\News;
use App\Http\Controllers\Newsletters;

class About extends Controller
{
    public function showAbout(){
        $team = app('App\Http\Controllers\Team')->getAllMembers();
        return view('about')
        ->with('team', $team);
    }
}
