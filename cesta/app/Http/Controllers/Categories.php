<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Categories extends Controller
{
    public function getAllCategories(){
        $allCategories = DB::table('tabela_categorias')->get();
        return $allCategories;
    }

    public function getName($id){
        $categorie = DB::table('tabela_categorias')->find($id);
        return $categorie;
    }
}
