<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\CommentsModel;

class Comments extends Controller
{

    public function getAllComments($type, $id)
    {
        return CommentsModel::getAllComments($type, $id);
    }

    public function getAmount($type, $id){
        return DB::table('tabela_comentarios')
                    ->where('tipo','=',$type)
                    ->where('id_post','=', $id)
                    ->count();
    }

    public function recordComment(Request $request)
    {
        date_default_timezone_set('america/sao_paulo');
        $date = date('Y/m/d h:i:s', time());
        DB::table('tabela_comentarios')->insert(
            [
                'nome' => $request->name,
                'email' => $request->email,
                'comentario' => $request->message,
                'id_comentario' => $request->commentId,
                'resposta' => $request->commentType,
                'tipo' => $request->type,
                'id_post' => $request->idPost,
                'data' => $date
            ]
        );
        return 1;
    }
}
