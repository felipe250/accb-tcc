<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Categories;

class Contact extends Controller
{

    public function storeMessage(Request $request){
        DB::table('tabela_mensagens')->insert(
            ['nome' => $request->input('name'), 
            'email' => $request->input('email'), 
            'assunto' => $request->input('subject'), 
            'mensagem' => $request->input('message')]
        );
        return view("contact");
    }
}
