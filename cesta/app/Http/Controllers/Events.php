<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class Events extends Controller
{

    public function showAll(){
        $allEvents = DB::table('tabela_eventos')->orderBy('data_inicio')->get();
        return view('events')->with('allEvents', $allEvents);
    }

}
