<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\News;
use App\Http\Controllers\Newsletters;

class Home extends Controller
{
    public function showHome(){
        $latestNews = app('App\Http\Controllers\News')->getLatestNews();
        $latestNewsletters = app('App\Http\Controllers\Newsletters')->getLatestNewsletters();
        $team = app('App\Http\Controllers\Team')->getAllMembers();
        return view('home')
        ->with('latestNews', $latestNews)
        ->with('latestNewsletters', $latestNewsletters)
        ->with('team', $team);
    }
}
