<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Categories;
use App\Models\CommentsModel;

class News extends Controller
{
    private const newsPerPage = 4;

    public function showAll(){
        $allNews = DB::table('tabela_noticias')->latest('data')->paginate($this::newsPerPage);
        $popularNews = $this::getPopularNews();
        foreach($allNews as $key => $a){
            $allNews[$key]->count = CommentsModel::getAmount(1, $a->id);
        }
        return view('news')
        ->with('allNews', $allNews)
        ->with('popularNews', $popularNews)
        ->with('allCategories', app('App\Http\Controllers\Categories')->getAllCategories());
        
    }

    public function getAllNewsByCategorie($id){
        $allNews = DB::table('tabela_noticias')->latest('data')->where('categoria_id', $id)->paginate($this::newsPerPage);
        foreach($allNews as $key => $a){
            $allNews[$key]->count = CommentsModel::getAmount(1, $a->id);
        }
        return view('news')
        ->with('allNews', $allNews)
        ->with('popularNews', $this::getPopularNews())
        ->with('allCategories', app('App\Http\Controllers\Categories')->getAllCategories());
    }

    public function getLatestNews(){
        $latestNews = DB::table('tabela_noticias')->latest('data')->limit(4)->get();
        return $latestNews;
    }

    public function getPopularNews(){
        $popularNews = DB::table('tabela_noticias')->orderBy('visualizacoes', 'desc')->limit(4)->get();
        return $popularNews;
    }

    public function getNewsById($id){
        $news = DB::table('tabela_noticias')->find($id);

        if(empty($news))
            echo "Error 404";
        else
            self::incrementViews($id);
           return view('singleNews')
           ->with('news', $news)
           ->with('latestNews', $this::getLatestNews())
           ->with('allComments', app('App\Http\Controllers\Comments')->getAllComments(1, $id))
           ->with('allCategories', app('App\Http\Controllers\Categories')->getAllCategories());
        
    }

    public function incrementViews($id){
        DB::table('tabela_noticias')
        ->where('id', $id)
        ->increment('visualizacoes');
    }

    public function searchNews (Request $request){
        $allNews = DB::table('tabela_noticias')->where('titulo', 'like', '%'.$request->input("search").'%')->paginate($this::newsPerPage);
        return view('news')
        ->with('allNews', $allNews)
        ->with('popularNews', $this::getPopularNews())
        ->with('allCategories', app('App\Http\Controllers\Categories')->getAllCategories());
    }
}
