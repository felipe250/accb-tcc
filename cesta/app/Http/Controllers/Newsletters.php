<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Categories;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use App\Models\CommentsModel;

class Newsletters extends Controller
{

    public function showAll(){
        $allNewsletters = DB::table('tabela_boletins')->where('ano', now()->year)->latest('data')->get();
        $allYears = DB::table('tabela_boletins')->select('ano')->groupBy('ano')->orderBy('ano', 'desc')->get();
        
        foreach($allNewsletters as $key => $a){
            $allNewsletters[$key]->count = CommentsModel::getAmount(0, $a->id);
        }
        return view('newsletters')
        ->with('allNewsletters', $allNewsletters)
        ->with('allYears', $allYears);
    }

    public function getLatestNewsletters(){
        $latestNewsletters = DB::table('tabela_boletins')->latest('data')->limit(3)->get();
        foreach($latestNewsletters as $key => $a){
            $latestNewsletters[$key]->count = CommentsModel::getAmount(0, $a->id);
        }
        return $latestNewsletters;
    }

    public function getNewslettersByYear($year){
        $array =  DB::table('tabela_boletins')->where('ano', $year)->latest('data')->get();
        foreach($array as $key => $a){
            $array[$key]->count = CommentsModel::getAmount(0, $a->id);
        }
        return $array;
        
    }
    public function getNewslettersById($month, $year, $id){
        $newsletter = DB::table('tabela_boletins')->find($id);
        $categorieName = "Nome categoria";//app('App\Http\Controllers\Categories')->getName($id)->nome;

        if(empty($newsletter))
            echo "Error 404";
        else
            self::incrementViews($id);
           return view('singleNewsletter')
           ->with('newsletter', $newsletter)
           ->with('allComments', app('App\Http\Controllers\Comments')->getAllComments(0, $id))
           ->with('categorie', $categorieName);
        
    }

    public function incrementViews($id){
        DB::table('tabela_boletins')
        ->where('id', $id)
        ->increment('visualizacoes');
    }
}
