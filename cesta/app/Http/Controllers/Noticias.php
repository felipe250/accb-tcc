<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Categories;

class Noticias extends Controller
{
    public function showAll(){
        return view('news');
        
    }

    public function getNewsById($id){
        $news = DB::table('tabela_noticias')->find($id);
        $categorieName = app('App\Http\Controllers\Categories')->getName($id)->nome;

        if(empty($news))
            echo "Error 404";
        else
           return view('singleNews')
           ->with('news', $news)
           ->with('categorie', $categorieName);
        
    }
}
