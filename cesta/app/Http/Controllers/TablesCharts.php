<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Categories;
use App\Models\TablesChartsModel;

class TablesCharts extends Controller
{

    public function getData(Request $request){
    	$monthStart = $request->period['monthStart'];
    	$yearStart = $request->period['yearStart'];
    	$monthFinish = $request->period['monthFinish'];
    	$yearFinish = $request->period['yearFinish'];
    	$products = $request->products;
    	$cities = $request->citys;
    	$queryType = $request->queryType;
    	$interval = $request->period['interval'];

    	return TablesChartsModel::getData($monthStart, $yearStart, $monthFinish, $yearFinish, $products, $cities, $queryType, $interval);
    }
}
