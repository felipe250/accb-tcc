<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\News;
use App\Http\Controllers\Newsletters;

class Team extends Controller
{
    public function getAllMembers(){
        $team = DB::table('tabela_equipe')->get();
        return $team;
    }
}
