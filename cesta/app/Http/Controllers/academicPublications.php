<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class academicPublications extends Controller
{
    private const publicationsPerPage = 10;

    public function showAll(){
        $allPublications = DB::table('tabela_publicacoes')->latest('data')->paginate($this::publicationsPerPage);
        return view('academicPublications')->with('allPublications', $allPublications);
    }

    public function searchPublications (Request $request){
        $allPublications = DB::table('tabela_publicacoes')->where('titulo', 'like', '%'.$request->input("search").'%')->paginate($this::publicationsPerPage);
        return view('academicPublications')
        ->with('allPublications', $allPublications);
    }

    public function getPublicationById($id){
        $publication = DB::table('tabela_publicacoes')->find($id);

        if(empty($publication))
            echo "Error 404";
        else
           return view('singleAcademicPublication')
           ->with('allComments', app('App\Http\Controllers\Comments')->getAllComments(2, $id))
           ->with('publication', $publication);
        
    }
}
