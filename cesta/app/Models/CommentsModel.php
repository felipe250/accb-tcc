<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CommentsModel extends Model
{
    public static function getAmount ($type, $id){
        $Amount = DB::table('tabela_comentarios')
                    ->where('tipo','=',$type)
                    ->where('id_post','=', $id)
                    ->count();
        return $Amount;
    }

    public static function getAllComments($type, $id){
    	// $allComments = DB::table('tabela_comentarios')->where([['tipo',0] , ['id_post', $id]])->latest('data')->get();
        $comments = DB::table('tabela_comentarios')->where([['tipo', $type], ['id_post', $id], ['resposta', 0]])->orderBy('data')->get();
        $answers = DB::table('tabela_comentarios')->where([['tipo', $type], ['id_post', $id], ['resposta', 1]])->orderBy('data')->get();
        $allComments = $array = ['comments' => $comments, 'answers' => $answers, 'count' => count($comments) + count($answers)];
        return $allComments;
    }
}