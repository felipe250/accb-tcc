<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class NewsModel extends Model
{
    public static function getAmount ($type, $id){
        $Amount = DB::table('tabela_comentarios')
                    ->where('tipo','=',$type)
                    ->where('id_post','=', $id)
                    ->count();
        return $Amount;
    }
}