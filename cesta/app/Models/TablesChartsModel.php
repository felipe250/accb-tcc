<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TablesChartsModel extends Model
{
    public static function getData ($monthStart, $yearStart, $monthFinish, $yearFinish, $products, $cities, $queryType, $interval){
    	$periods = array();
    	$result = array();

    	//Organizar Produtos
    	if($queryType == "total"){
    		array_push($result, ['productName' => 'Carne', 'productID' => 1, 'periods' => array()], ['productName' => 'Leite', 'productID' => 2, 'periods' => array()], ['productName' => 'Feijão', 'productID' => 3, 'periods' => array()], ['productName' => 'Arroz', 'productID' => 4, 'periods' => array()], ['productName' => 'Farinha', 'productID' => 5, 'periods' => array()], ['productName' => 'Tomate', 'productID' => 6, 'periods' => array()], ['productName' => 'Pão', 'productID' => 7, 'periods' => array()], ['productName' => 'Café', 'productID' => 8, 'periods' => array()], ['productName' => 'Banana', 'productID' => 9, 'periods' => array()], ['productName' => 'Açúcar', 'productID' => 10, 'periods' => array()], ['productName' => 'Óleo', 'productID' => 11, 'periods' => array()], ['productName' => 'Manteiga', 'productID' => 12, 'periods' => array()]);
    	}else{
	    	foreach ($products as $key_product => $product) {

	    			if($product == 'carne')
	    				array_push($result, ['productName' => 'Carne', 'productID' => 1, 'periods' => array()]);

	    			if($product == 'leite')
	    				array_push($result, ['productName' => 'Leite', 'productID' => 2, 'periods' => array()]);

	    			if($product == 'feijao')
	    				array_push($result, ['productName' => 'Feijão', 'productID' => 3, 'periods' => array()]);

	    			if($product == 'arroz')
	    				array_push($result, ['productName' => 'Arroz', 'productID' => 4, 'periods' => array()]);

	    			if($product == 'farinha')
	    				array_push($result, ['productName' => 'Farinha', 'productID' => 5, 'periods' => array()]);

	    			if($product == 'tomate')
	    				array_push($result, ['productName' => 'Tomate', 'productID' => 6, 'periods' => array()]);

	    			if($product == 'pao')
	    				array_push($result, ['productName' => 'Pão', 'productID' => 7, 'periods' => array()]);

	    			if($product == 'cafe')
	    				array_push($result, ['productName' => 'Café', 'productID' => 8, 'periods' => array()]);

	    			if($product == 'banana')
	    				array_push($result, ['productName' => 'Banana', 'productID' => 9, 'periods' => array()]);

	    			if($product == 'acucar')
	    				array_push($result, ['productName' => 'Açúcar', 'productID' => 10, 'periods' => array()]);

	    			if($product == 'oleo')
	    				array_push($result, ['productName' => 'Óleo', 'productID' => 11, 'periods' => array()]);

	    			if($product == 'manteiga')
	    				array_push($result, ['productName' => 'Manteiga', 'productID' => 12, 'periods' => array()]);

	    	}
	    }

    	//Montar Períodos
	
		$monthStart = intval($monthStart);
    	$yearStart = intval($yearStart);
    	$monthFinish = intval($monthFinish);
    	$yearFinish = intval($yearFinish); 
    	$interval = intval($interval);

    	$flag = true;
    	for ($year=$yearStart; $year <= $yearFinish; $year++) { 
    		$auxStart = $flag ? $monthStart : 1;
    		$auxFinish = $year == $yearFinish ? $monthFinish : 12;

    		for ($month = $auxStart; $month <=  $auxFinish; $month++) { 
    			array_push($periods, ['month' => $month, 'year' => $year]);
    		}
    		$flag = false;
    	}

    	$return = array();

    	//Buscar Dados no Banco
    	foreach ($cities as $key_city => $city) {
    		array_push($return, ['city' => $city , 'products' => $result]);

    		$cityID = $city == 'ilheus'? 2 : 1;
	    	foreach ($result as $key_product => $product) {
	    		foreach ($periods as $key_period => $period) {
	    			//Pegar ID da pesquisa

	    			$pesquisaID = DB::table('tabela_pesquisas')
	    								->select('pesquisa_id')
	    								->whereMonth('pesquisa_data', $period['month'])
	    								->whereYear('pesquisa_data', $period['year'])
	    								->first();
	    			if(is_null($pesquisaID)){
	    				continue;
	    			}

	    			//Pegar Valores dos produtos
	    			$productData = DB::table('tabela_pesquisa_resultados_produtos')
	    								->select()
	    								->where([
	    									['pesquisa_id', $pesquisaID->pesquisa_id],
	    									['cidade_id', $cityID],
	    									['produto_id', $product['productID']]
	    								])
	    								->first();

	    			
	    			switch ($queryType) {
	    				case 'mensal':
	    					$value = $productData->produto_preco_total;
	    					break;
	    				case 'medio':
	    					$value = $productData->produto_preco_medio;
	    					# code...
	    					break;

	    				case 'trabalho':
	    					$value = $productData->produto_tempo_trabalho;
	    					break;
	    				
	    				case 'total':
	    					$value = $productData->produto_preco_total;
	    					break;

	    				default:
	    					# code...
	    					break;
	    			}

	    			switch ($period['month']) {
	    				case 1:
	    					$periodName = 'Jan';
	    					break;
	    				case 2:
	    					$periodName = 'Fev';
	    					break;
	    				case 3:
	    					$periodName = 'Mar';
	    					break;
	    				case 4:
	    					$periodName = 'Abr';
	    					break;
	    				case 5:
	    					$periodName = 'Mai';
	    					break;
	    				case 6:
	    					$periodName = 'Jun';
	    					break;
	    				case 7:
	    					$periodName = 'Jul';
	    					break;
	    				case 8:
	    					$periodName = 'Ago';
	    					break;
	    				case 9:
	    					$periodName = 'Set';
	    					break;
	    				case 10:
	    					$periodName = 'Out';
	    					break;
	    				case 11:
	    					$periodName = 'Nov';
	    					break;
	    				case 12:
	    					$periodName = 'Dez';
	    					break;
	    				
	    				default:
	    					# code...
	    					break;
	    			}

	    			$periodName .= '/'.$period['year'];

	    			array_push($return[$key_city]['products'][$key_product]['periods'], ['value' => $value, 'periodName' => $periodName]);

	    		}
	    	}
	    }

	    return $return;
        
    }
}