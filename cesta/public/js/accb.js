var Accb = (function() {

	return {

		index: function() {

		},

		chartsTables: function(){
			/*Listeners*/
			$('#products').select2();

			$('#exportAllData').on('click', function(){
				alert("Esta função estará disponível em breve.");
			});

			//Gerar Gráficos e Tabelas
			$('#generateTableChart').on('click', function(){
				var parameters = Accb.getParameters();

				if(parameters === false){
					return;
				}
				var url = 'graficosetabelas/getData';
				$.ajax({
					async: true,
					type: "GET",
					url: url,
					data: parameters,
					success: function(data) {
						if(parameters.displayMode == 'table'){
							Accb.makeTable(data);
						}else{
							Accb.makeChart(data);
						}
					},
					error: function() {
						alert("O servidor está demorando mais do que o normal para responder. Por favor, tente novamente.");
					}
				});


				
			});
			/*Listeners*/

			

		},

		getParameters: function(){

			$('.alertform').remove();

			//Tipo
			var queryType = $('input[name=queryType]:checked').val();

			//Cidade
			var citys = [];
			var ilheus = $('#ilheus').is(":checked");
			var itabuna = $('#itabuna').is(":checked");

			if(ilheus || itabuna){
				if(ilheus){
					citys.push('ilheus');
				}
				if (itabuna) {
					citys.push('itabuna');
				}
			}else{
				$('#ilheus').focus();
				$('#itabuna').focus();
				$('#citys').append('<div class="alertform d-flex justify-content-center"><font style="color: red">*Campo obrigatório*</font></div>');
				return false;
			}

			//Período
			var monthStart = $( "#monthStart option:selected" ).text();
			var yearStart = $( "#yearStart option:selected" ).text();
			var monthFinish = $( "#monthFinish option:selected" ).text();
			var yearFinish = $( "#yearFinish option:selected" ).text();
			var interval = $( "#interval option:selected" ).text();

			//Produtos

			var products = $('#products').val();

			//Modo de exibição

			var displayMode = $('input[name=resultType]:checked').val();

			return {
				'queryType': queryType,
				'citys': citys,
				'period': {
					'monthStart': monthStart,
					'yearStart': yearStart,
					'monthFinish': monthFinish,
					'yearFinish': yearFinish,
					'interval': interval
				},
				'products': products,
				'displayMode': displayMode
			};
		},

		makeChart: function(data){
			//Remover Tabela se Existir

			$('#dataTable').html('');

			var letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q'];
			var colors = ['rgba(0, 191, 255, 1)', 'rgba(0, 205, 102, 1)', 'rgba(255, 140, 0, 1)', 'rgba(255, 48, 48, 1)', 'rgba(238, 238, 0)', 'rgba(122, 55, 139)', 'rgba(238, 210, 238)', 'rgba(153, 50, 204)', 'rgba(34, 139, 34)', 'rgba(47, 79, 79)', 'rgba(205, 92, 92)', 'rgba(0, 0, 205)'];

			data.forEach(function(city, key_city){
				var labels = [];
				var dataset = [];
				var options = [];
				var labels = [];
				var min = 999999999;
				var max = 0;

				city.products.forEach(function(product, key_product){
					var values = [];
					labels = [];
					product.periods.forEach(function(period, key_period){
						values.push(period.value);
						labels.push(period.periodName);

						if(period.value > max)
							max = period.value;
						if(period.value < min)
							min = period.value;
					});
					//dataset
					dataset.push(
							{
								label: product.productName,
								yAxisID: letters[key_product],
								data: values,
								backgroundColor: colors[key_product]
							}
						);
					//option
					options.push(
						{
							id: letters[key_product],
							display: false,
							ticks: {
					        	callback: function(value, index, values) {
			                        return 'R$' + value;
			                    }
			                }
					    }
					);


				});

				var queryType = $('input[name=queryType]:checked').val();

				if(queryType == 'total'){
					//Calcular Totais
					var dataChart = [];
					city.products[0].periods.forEach(function(period, key_period){
						var total = 0;
						city.products.forEach(function(product, key_product){
							total += parseFloat(product.periods[key_period].value);
						});
							dataChart.push(total);
					});

					dataChart.forEach(function(dataValue){
						if(dataValue > max){
							max = dataValue;
						}
					});
					max+= 10 - (max%1);




					//dataset
					dataset = [];
					dataset.push(
							{
								label: 'Total',
								yAxisID: letters[0],
								data: dataChart,
								backgroundColor: colors[0]
							}
						);
					//option
					options = [];
					options.push(
						{
							id: letters[0],
							display: false,
							ticks: {
					        	callback: function(value, index, values) {
			                        return 'R$' + value;
			                    }
			                }
					    }
					);



				}

				
				//Ajustar Options
				options[0].display = 'auto';
				options[0].ticks.min = 0;
				options[0].ticks.max = max;

				//Montar Chart
				if(data.length > 1){
					if(key_city == 0){
						$("#chartJS").html('<canvas id="Chart'+city.city+'"></canvas>');
					}else{
						$("#chartJS").append('<br><canvas id="Chart'+city.city+'"></canvas>');
					}
				}else{
					$("#chartJS").html('<canvas id="Chart'+city.city+'"></canvas>');
				}
				var ctx = $('#Chart'+city.city+'');
				var myChart = new Chart(ctx, {
					type: 'bar',
					data: {
						labels: labels,
						datasets: dataset
					},
					options: {
				        title: {
				            display: true,
				            position: 'top',
				            text: city.city == 'ilheus' ? 'Ilhéus' : 'Itabuna',
				            fontSize: 20
				        },

						legend: {
				            display: true,
				            position: 'bottom'
				        },

						scales: {
							yAxes: options
						}
					}
				});

				// console.log(dataset);
				// console.log(options);
				// console.log(labels);
				// return;
			});
			//return;

			


			// $("#chartJS").html('<canvas id="myChart"></canvas>');


			// var ctx = $('#myChart');
			// var myChart = new Chart(ctx, {
			// 	type: 'bar',
			// 	data: {
			// 		labels: ['Jan 2020', 'Fev 2020', 'Mar 2020', 'Abr 2020', 'Mai 2020', 'Jun 2020', 'Jul 2020', 'Ago 2020', 'Set 2020'],
			// 		datasets: [{
			// 			label: 'Açúcar',
			// 			yAxisID: 'A',
			// 			data: [7.17, 7.32, 7.95, 8.07, 8.01, 8.34, 7.08, 8.2, 8.52],
			// 			backgroundColor: 'rgba(0, 191, 255, 1)'
			// 		}, {
			// 			label: 'Arroz',
			// 			yAxisID: 'B',
			// 			data: [11.17, 11.32, 11.95, 12.07, 12.01, 12.34, 11.08, 12.2, 14.52],
			// 			backgroundColor: 'rgba(0, 205, 102, 1)'
			// 		}, {
			// 			label: 'Banana',
			// 			yAxisID: 'C',
			// 			data: [47.17, 47.32, 47.95, 48.07, 48.01, 48.34, 47.08, 48.2, 48.52],
			// 			backgroundColor: 'rgba(255, 140, 0, 1)'
			// 		}, {
			// 			label: 'Carne',
			// 			yAxisID: 'D',
			// 			data: [107.17, 117.32, 118.95, 118.07, 118.01, 118.34, 127.08, 138.2, 148.52],
			// 			backgroundColor: 'rgba(255, 48, 48, 1)'
			// 		}]
			// 	},
			// 	options: {
			//         title: {
			//             display: true,
			//             position: 'top',
			//             text: 'Ilhéus',
			//             fontSize: 20
			//         },

			// 		legend: {
			//             display: true,
			//             position: 'bottom'
			//         },

			// 		scales: {
			// 			yAxes: [{
			// 				id: 'A',
			// 				display: 'auto',
			// 				ticks: {
			// 		        	max: 150,
			// 		        	min: 0,
			// 		        	callback: function(value, index, values) {
			//                         return 'R$' + value;
			//                     }
			// 		        }
			// 			}, {
			// 				id: 'B',
			// 				display: false
			// 			}, {
			// 				id: 'C',
			// 				display: false
			// 			}, {
			// 				id: 'D',
			// 				display: false
			// 			}]
			// 		}
			// 	}
			// });
		},

		makeTable: function(data){
			//Fechar Gráficos
			$('#chartJS').html('');

			var queryType = $('input[name=queryType]:checked').val();

			console.log(data);


			data.forEach(function(city, key_city){
				var dataSet = [];

				//Definir datasets
				city.products[0].periods.forEach(function(period, key_period){
					var line = [];
					var total = 0;
					var aux = [];
					line.push(period.periodName);
					aux.push(period.periodName);
					city.products.forEach(function(product, key_product){
						line.push(product.periods[key_period].value);
						total += parseFloat(product.periods[key_period].value);
						console.log(parseFloat(product.periods[key_period].value));
					});
					if(queryType == 'total'){
						aux.push(total);
						dataSet.push(aux);
					}else{
						dataSet.push(line);
					}
				});

				//Definir colunas
				var columns = [];

				columns.push({ title: "Período" });

				if(queryType == 'total'){
					columns.push({ title: 'Total' , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )});
				}else{
					city.products.forEach(function(product, key_product){
						columns.push({ title: product.productName , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )});
					});
				}


				//Ajustar Nome
				var nameCity = city.city == 'ilheus' ? 'Ilhéus' : 'Itabuna';

				//Montar Tabelas

				if(data.length > 1){
					if(key_city == 0){
						$("#dataTable").html('<center><h3>'+nameCity+'</h3></center><table class="table table-striped table-bordered" id="resultTable'+city.city+'" style="width: 100%"></table>');
					}else{
						$("#dataTable").append('<br><center><h3>'+nameCity+'</h3></center><table class="table table-striped table-bordered" id="resultTable'+city.city+'" style="width: 100%"></table>');
					}
				}else{
					$("#dataTable").html('<center><h3>'+nameCity+'</h3></center><table class="table table-striped table-bordered" id="resultTable'+city.city+'" style="width: 100%"></table>');
				}


				console.log(columns);
				console.log(dataSet);
				

				$('#resultTable'+city.city).DataTable( {
			        data: dataSet,
			        columns: columns,
			        paging: false,
			        searching: false,
			        "ordering": false,

			        "language": {
			        	 "sSearch": "Buscar:",
			        	 "paginate": {
					        "first":      "Primeira",
					        "last":       "última",
					        "next":       "Próxima",
					        "previous":   "Anterior"
					    },
					    "info": "Mostrando _START_ - _END_ dos _TOTAL_ resultados",
					    "lengthMenu":     "Visualizando _MENU_ entries",
			        }
			    });





			});

			// $("#dataTable").html('<table class="table table-striped table-bordered" id="resultTable" style="width: 100%"></table>');
			// //$('#resultTable').DataTable();

			// var dataSet = [
			//     [ "Jan - 2020", 7.17, 7.80, 47.17, 107.0],
			//     [ "Fev - 2020", 7.37, 8.01, 47.68, 115.1],
			//     [ "Mar - 2020", 7.57, 8.52, 46.87, 118.8],
			//     [ "Abr - 2020", 7.97, 9.80, 45.94, 120.5],
			//     [ "Mai - 2020", 8.17, 10.20, 44.45, 128.2],
			//     [ "Jun - 2020", 8.15, 11.90, 43.56, 136.8],
			//     [ "Jul - 2020", 7.17, 12.80, 42.94, 138.4],
			//     [ "Ago - 2020", 7.01, 14.80, 45.95, 140.8],
			//     [ "Set - 2020", 7.48, 14.52, 48.17, 148.52],
			// ];

			// $('#resultTable').DataTable( {
		 //        data: dataSet,
		 //        columns: [
		 //            { title: "Período" },
		 //            { title: "Açúcar" , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )},
		 //            { title: "Arroz" , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )},
		 //            { title: "Banana" , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )},
		 //            { title: "Carne" , render: $.fn.dataTable.render.number( '.', ',', 2, 'R$ ' )}
		 //        ],
		 //        paging: false,
		 //        searching: false,

		 //        "language": {
		 //        	 "sSearch": "Buscar:",
		 //        	 "paginate": {
			// 	        "first":      "Primeira",
			// 	        "last":       "última",
			// 	        "next":       "Próxima",
			// 	        "previous":   "Anterior"
			// 	    },
			// 	    "info": "Mostrando _START_ - _END_ dos _TOTAL_ resultados",
			// 	    "lengthMenu":     "Visualizando _MENU_ entries",
		 //        }
		 //    });
		}
	}
})(Accb);