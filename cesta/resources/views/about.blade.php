@extends('layout.accb')

@section('titulo', 'Sobre')

@section('conteudo')
        <!--================About Area =================-->
        <section class="about_area p_120">
        	<div class="container">
        		<div class="row about_inner">
        			<div class="col-lg-6">
						<div class="accordion" id="accordionExample">
							
							<div class="section-top-border">
								<h3 class="mb-30 title_color">O que fazemos</h3>
								<div class="row">
									<div class="col-lg-12">
										<blockquote class="generic-blockquote">
										No o Projeto são feitas pesquisas mensais de levantamento de preços de dos 12 itens 
										que compõem a ração mínima essencial, conforme artigo 2º. do Decreto-Lei 399, de 30 de 
										abril de 1938, conhecida popularmente como Cesta Básica, nas cidades baianas de Ilhéus 
										e Itabuna. Além dos itens dessa Cesta, pesquisamos também itens de higiene pessoal e 
										limpeza doméstica.
										Mensalmente, acompanhamos a evolução dos preços desses itens, estimando gasto mensal e 
										tempo de trabalho necessário por item e o custo total da cesta básica oficial. Essas 
										informações são sistematizadas, analisadas e divulgadas mensalmente sob a forma de 
										boletins e releases, os quais são enviados para meios de comunicação, a fim de facilitar 
										o acesso às informações.O projeto também serve como um "laboratório" para os estudantes 
										da UESC aplicarem conhecimentos sobre economia, a partir do envolvimento como bolsista, 
										melhorando sua formação acadêmica.
										</blockquote>
									</div>
								</div>
							</div>

							<div class="section-top-border">
								<h3 class="mb-30 title_color">A história do projeto</h3>
								<div class="row">
									<div class="col-lg-12">
										<blockquote class="generic-blockquote">
											O Projeto ACCB é um projeto de extensão do Departamento de Ciências Econômicas 
											da Universidade Estadual de Santa Cruz, iniciado em março de 1999, sob a coordenação 
											do Prof. Edmir Menezes Santos. Em 2004, assume a coordenação a Prof. Mônica de Moura 
											Pires, reformulando totalmente o projeto, o qual passou a adotar a mesma metodologia 
											de coleta do DIEESE, ampliou-se o número de estabelecimentos inseridos nas coletas de 
											preço, foi elaborado um boletim mensal para divulgação das análises econômicas entre 
											outras ações. Em 2008 foi criada uma página do projeto, tornando possível a consulta 
											online das informações divulgadas nos boletins e disponibilizando um banco de dados, 
											expandido o nível de alcance das informações produzidas no projeto. A partir do Projeto 
											ACCB já foram produzidos diversos trabalhos técnico-científicos, fruto da credibilidade 
											e seriedade do nosso trabalho.<br> 
											Estamos agora em nova etapa, buscando ampliar o nível de alcance das informações que 
											produzimos, atraindo novos leitores para o nosso site.
										</blockquote>
									</div>
								</div>
							</div>

							<div class="section-top-border">
								<h3 class="mb-30 title_color">Metodologia</h3>
								<div class="row">
									<div class="col-lg-12">
										<blockquote class="generic-blockquote">
										O levantamento de preços é feito nos estabelecimentos de maior fluxo de compra das 
										cidades pesquisadas. O preço dos produtos é obtido diretamente da prateleira. Após 
										coleta, esses dados são tabulados e submetidos a análises.<br>
										Quando o produto possui várias cotações, coletam-se todas elas, sendo retirados os 
										valores extremos e, estabelecendo-se três níveis de preço, faz-se uma média aritmética 
										desses preços. Posteriormente, somam-se os vários resultados coletados nos estabelecimentos 
										comerciais e obtém-se, assim, o preço médio para cada produto. Este preço multiplicado pelas 
										quantidades determina o gasto mensal do trabalhador com cada produto, cuja soma resulta no 
										custo mensal da Cesta Básica para um indivíduo adulto.<br><br>
										O cálculo da quantidade de horas de trabalho necessárias para que o trabalhador que recebe 
										salário mínimo possa adquirir a cesta básica de alimentos é feito tomando-se como base a 
										quantidade de 220 horas/mês, conforme determina a Constituição Federativa da República do 
										Brasil de 1988. Assim, estabelece-se o tempo de trabalho necessário para cada item e o total 
										da cesta básica. A fórmula é a seguinte:<br><br>
										<div class="row coll-lg-12 d-flex justify-content-center">
											(Salário mínimo)/220 = (Custo da cesta)/X<br>

											X = (Custo da cesta/salário mínimo) * 220
										</div>
										</blockquote>
									</div>
								</div>
							</div>

							

						</div>
        			</div>
        			<div class="col-lg-6">
        				

						<div class="section-top-border">
							<h3 class="mb-30 title_color">Público Alvo</h3>
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
									Nosso público alvo são os interessados sobre a temática cesta básica e que buscam acompanhar o 
									comportamento e a evolução dos preços dos produtos dessa cesta. E também estudantes, professores, 
									pesquisadores e a imprensa que nos ajuda na divulgação das informações contidas nos boletins e 
									releases mensais.
									</blockquote>
								</div>
							</div>
						</div>

						<div class="section-top-border">
							<h3 class="mb-30 title_color">As informações</h3>
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
									Nosso site permite disseminar conhecimento na área de economia junto a estudantes, 
									professores e interessados sobre o tema. Auxilia também no desenvolvimento de trabalhos 
									nos cursos de graduação e pós-graduação a partir do uso do banco de dados do projeto e 
									gestores das cidades pesquisadas na elaboração de políticas públicas. Assim, fortalecemos 
									as relações entre a comunidade acadêmica e a sociedade.
									</blockquote>
								</div>
							</div>
						</div>

						<div class="section-top-border">
							<h3 class="mb-30 title_color">O que faz o bolsista</h3>
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
									<ul class="unordered-list">
										<li>Coleta preços dos itens da cesta - 28 estabelecimentos (14 em Ilhéus e 14 em Itabuna).</li>
										<li>Tabula os dados coletados em programa desenvolvido em parceria com o DCET.</li>
										<li>Estima o custo da cesta básica, gasto mensal, tempo de trabalho, variação mensal, semestral e anual de cada produto e total dos itens da cesta.</li>
										<li>Elabora boletins mensais e release.s</li>
										<li>Publica e divulga o boletim mensal na nossa página.</li>
									</ul>
									</blockquote>
								</div>
							</div>
						</div>

						<div class="section-top-border">
							<h3 class="mb-30 title_color">DIEESE</h3>
							<div class="row">
								<div class="col-lg-12">
									<blockquote class="generic-blockquote">
									<a href="https://www.dieese.org.br/" target="_blank">Acessar DIEESE</a>
									</blockquote>
								</div>
							</div>
						</div>





        			</div>
        		</div>
        		
        	</div>
        </section>
        <!--================End About Area =================-->
		@component('components.team', ['team'=>$team])
		@endcomponent
        @endsection