@extends('layout.accb')

@section('titulo', 'Publicações Acadêmicas')

@section('conteudo')

<section class="courses_area p_120">
  <div class="container">
    <div class="main_title">
      <h2>Publicações Acadêmicas ACCB/UESC</h2>
    </div>
    <div class="row">
      <div class="col">
        <div class="row d-flex justify-content-center pb-3">
          <div class="col-lg-4 d-flex justify-content-center">
            <form action="" method="POST">
              @csrf
              <div class="form-row align-items-center">
                <div class="col-auto">
                  <input type="text" name = "search" class="form-control" id="formGroupExampleInput" placeholder="Buscar Publicação">
                </div>
                <div class="col-auto">
                  <button type="submit" class="genric-btn success circle"><span class="lnr lnr-magnifier"></span></button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @if($allPublications->isEmpty())
        <div class="alert alert-danger d-flex justify-content-center" role="alert">
          Nenhuma publicação encontrada.
        </div>
        @else
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="80%" scope="col">Título</th>
              <th width="15%" scope="col">Data da Publicação</th>
              <th width="5%" scope="col"></th>
            </tr>
          </thead>
          <tbody>
            @foreach ($allPublications as $publication)
            <tr>
              <th scope="row">{{$publication->titulo}}</th>
              <td>{{$publication->data}}</td>
              <td align="center"><a href="{{route('singleAcademicPublication', ['id' => $publication->id])}}" class="genric-btn success circle arrow small">Ver<span class="lnr lnr-arrow-right"></span></a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
        {{$allPublications->links()}}
        @endif
      </div>
    </div>
  </div>
</section>
@endsection