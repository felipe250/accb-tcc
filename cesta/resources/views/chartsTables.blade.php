@extends('layout.accb')

@section('titulo', 'Tabelas e Gráficos')

@section('conteudo')

<section class="courses_area p_120">
  <div class="container">
    <div class="main_title">
      <h2>Tabelas e Gráficos ACCB/UESC</h2>
    </div>


    <div class="row">
        <div class="col-3">
          

              <div class="single-element-widget">
                <div class="d-flex justify-content-center">
                  <h3 class="mx-auto title_color">Tipo de Consulta</h3>
                </div>
                <div class="col-8">
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Gasto Mensal</p>
                    <div class="primary-radio">
                      <input type="radio" name="queryType" value="mensal" id="gastoMensal" checked="" style="opacity: 100">
                      <label for="gastoMensal"></label>
                    </div>
                  </div>
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Preço Médio</p>
                    <div class="primary-radio">
                      <input type="radio" name="queryType" value="medio" id="precoMedio" style="opacity: 100">
                      <label for="precoMedio"></label>
                    </div>
                  </div>
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Tempo de Trabalho</p>
                    <div class="primary-radio">
                      <input type="radio" name="queryType" value="trabalho" id="tempoTrabalho" style="opacity: 100">
                      <label for="tempoTrabalho"></label>
                    </div>
                  </div>
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Custo Total</p>
                    <div class="primary-radio">
                      <input type="radio" name="queryType" value="total" id="custoTotal" style="opacity: 100">
                      <label for="custoTotal"></label>
                    </div>
                  </div>
                </div>
              </div>
        </div>

        <div class="col-3" id="citys">
          

              <div class="single-element-widget">
                <div class="d-flex justify-content-center">
                  <h3 class="mx-auto title_color">Cidade</h3>
                </div>
                <div class="col-8">
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Ilhéus</p>
                    <div class="primary-checkbox">
                      <input type="checkbox" name="city" value="ilheus" id="ilheus" checked="">
                      <label for="ilheus"></label>
                    </div>
                  </div>
                  <div class="switch-wrap d-flex justify-content-between">
                    <p>Itabuna</p>
                    <div class="primary-checkbox">
                      <input type="checkbox" name="city" value="itabuna" id="itabuna" checked="">
                      <label for="itabuna"></label>
                    </div>
                  </div>
                </div>
              </div>
        </div>

        <div class="col-3">
          <div class="single-element-widget">
                <div class="d-flex justify-content-center">
                  <h3 class="mx-auto title_color">Período</h3>
                </div>

                <table width="100%">
                  <tr>
                    <td>
                      
                    </td>
                    <td align="center">
                      Mês
                    </td>
                    <td align="center">
                      Ano
                    </td>
                  </tr>

                  <tr>
                    <td>
                      De:
                    </td>
                    <td align="center">
                      <div class="col-12 d-flex justify-content-center">
                        <select id="monthStart">
                          <option value="1" selected="">01</option>
                          <option value="2">02</option>
                          <option value="3">03</option>
                          <option value="4">04</option>
                          <option value="5">05</option>
                          <option value="6">06</option>
                          <option value="7">07</option>
                          <option value="8">08</option>
                          <option value="9">09</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                      </div>
                    </td>
                    <td align="center">
                      <div class="col-12 d-flex justify-content-center">
                       <select id="yearStart">
                          <option value="2004">2004</option>
                          <option value="2005">2005</option>
                          <option value="2006">2006</option>
                          <option value="2007">2007</option>
                          <option value="2008">2008</option>
                          <option value="2009">2009</option>
                          <option value="2010">2010</option>
                          <option value="2011">2011</option>
                          <option value="2012">2012</option>
                          <option value="2013">2013</option>
                          <option value="2014">2014</option>
                          <option value="2015">2015</option>
                          <option value="2016">2016</option>
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019" selected="">2019</option>
                          <option value="2020">2020</option>
                        </select>
                      </div>
                    </td>
                  </tr>

                  <tr>
                    <td>
                      Até:
                    </td>
                    <td align="center">
                      <div class="col-12 d-flex justify-content-center">
                        <select id="monthFinish">
                          <option value="1">01</option>
                          <option value="2">02</option>
                          <option value="3">03</option>
                          <option value="4">04</option>
                          <option value="5">05</option>
                          <option value="6">06</option>
                          <option value="7">07</option>
                          <option value="8">08</option>
                          <option value="9" selected="">09</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                        </select>
                      </div>
                    </td>
                    <td>
                      <div class="col-12 d-flex justify-content-center">
                       <select id="yearFinish">
                          <option value="2004">2004</option>
                          <option value="2005" selected="">2005</option>
                          <option value="2006">2006</option>
                          <option value="2007">2007</option>
                          <option value="2008">2008</option>
                          <option value="2009">2009</option>
                          <option value="2010">2010</option>
                          <option value="2011">2011</option>
                          <option value="2012">2012</option>
                          <option value="2013">2013</option>
                          <option value="2014">2014</option>
                          <option value="2015">2015</option>
                          <option value="2016">2016</option>
                          <option value="2017">2017</option>
                          <option value="2018">2018</option>
                          <option value="2019" selected="">2019</option>
                          <option value="2020">2020</option>
                        </select>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      Resultados para: 
                    </td>
                    <td>
                      <div class="col-12 d-flex justify-content-center">
                      <select id="interval">
                          <option value="1">1 mês</option>
                          <option value="2">2 mês</option>
                          <option value="3">3 mês</option>
                          <option value="4">4 mês</option>
                          <option value="5">5 mês</option>
                          <option value="6">6 mês</option>
                        </select>
                      </div>



                    </td>
                  </tr>

                </table>

          </div>
        </div>

        <div class="col-3">
          

              <div class="single-element-widget">
                <div class="d-flex justify-content-center">
                  <h3 class="mx-auto title_color">Produtos</h3>
                </div>
                <select class="js-example-basic-multiple" id='products' name="states[]" multiple="multiple" style="width: 100%">
                  <option value="acucar" selected="">Açúcar</option>
                  <option value="arroz" selected="">Arroz</option>
                  <option value="banana" selected="">Banana</option>
                  <option value="cafe" selected="">Café</option>
                  <option value="carne" selected="">Carne</option>
                  <option value="farinha" selected="">Farinha</option>
                  <option value="feijao" selected="">Feijão</option>
                  <option value="leite" selected="">Leite</option>
                  <option value="manteiga" selected="">Manteiga</option>
                  <option value="oleo" selected="">Óleo</option>
                  <option value="pao" selected="">Pão</option>
                  <option value="tomate" selected="">Tomate</option>
                </select>
              </div>
        </div>
    </div>

    <div class="row">
      <h4 class="title_color">Modo de Exibição:</h4>
      <div class="col-1">
        
        <div class="switch-wrap d-flex justify-content-between">
          <p>Tabela</p>
          <div class="primary-radio">
            <input type="radio" name="resultType" value="table" id="tableType" checked="" style="opacity: 100">
            <label for="tableType"></label>
          </div>
        </div>
      </div>

      <div class="col-1">
        

        <div class="switch-wrap d-flex justify-content-between">
          <p>Gráfico</p>
          <div class="primary-radio">
            <input type="radio" name="resultType" value="chart" id="chartType" style="opacity: 100">
            <label for="chartType"></label>
          </div>
        </div>
      </div>

      <div class="col-8 d-flex justify-content-end">
        <button class="genric-btn success circle arrow" id="generateTableChart">Visualizar<span class="lnr lnr-arrow-right"></span></button>
      </div>
      <!-- <div class="col-2 d-flex justify-content-center">
        <button class="genric-btn success circle arrow" id="exportAllData">Exportar <span class="lnr lnr-arrow-down"></span></button>
      </div> -->
    </div>

    <div class="col-12 mt-2" id="chartJS" style="border: 1px solid;">
    </div>

    <div class="col-12 mt-2" id="dataTable">
      


    </div>
  </div>
</section>
<script type="text/javascript">
      Accb.chartsTables();
</script>
@endsection