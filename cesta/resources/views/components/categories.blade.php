                            <aside class="single-sidebar-widget tag_cloud_widget">
                                <h4 class="widget_title">Categorias</h4>
                                <ul class="list">
                                    @foreach ($allCategories as $categorie)
                                    <li><a href="{{route('getAllNewsByCategorie', ['id' => $categorie->id])}}">{{$categorie->nome}}</a></li>
                                    @endforeach
                                </ul>
                            </aside>