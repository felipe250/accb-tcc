<style>
    html {
        scroll-behavior: smooth;
    }
</style>


<div class="comments-area" id="comments">
    @if($allComments['count'] == 0)
    <h4>Nenhum Comentário</h4>
    @else
    <h4>{{$allComments['count']}} Comentários</h4>
    @foreach ($allComments['comments'] as $comment)
    <div class="comment-list">
        <div class="single-comment justify-content-between d-flex">
            <div class="user justify-content-between d-flex">
                <div class="thumb">
                    <img src="{{asset('img/blog/c1.png')}}">
                </div>
                <div class="desc">
                    <h5><a href="#">{{$comment->nome}}</a></h5>
                    <p class="date">{{date('d/m/Y - H:i', strtotime($comment->data))}}</p>
                    <p class="comment">
                        {{$comment->comentario}}
                    </p>
                </div>
            </div>
            <div class="reply-btn">
                <a href="#comment-form" onclick="setGlobalId({{$comment->id}}, '{{$comment->nome}}')" class="btn-reply text-uppercase">Responder</a>
            </div>
        </div>
    </div>
    @foreach($allComments['answers'] as $answer)
    @if($answer->id_comentario == $comment->id)
    <!-- Resposta-->
    <div class="comment-list left-padding">
        <div class="single-comment justify-content-between d-flex">
            <div class="user justify-content-between d-flex">
                <div class="thumb">
                    <img src="{{asset('img/blog/c2.png')}}" alt="">
                </div>
                <div class="desc">
                    <h5><a href="#">{{$answer->nome}}</a></h5>
                    <p class="date">{{date('d/m/Y - H:i', strtotime($answer->data))}}</p>
                    <p class="comment">
                    {{$answer->comentario}}
                    </p>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
    @endforeach
    @endif

</div>
<div id="comment-form" class="comment-form">
    <div class="row d-flex justify-content-center">
        <h4 id="titleComment">Deixe um Comentário</h4>
        <div id="buttonCancelAnswer"></div>
    </div>
    <form action="/cesta/public/recordComment" method="POST">
        @csrf
        <div class="form-group form-inline">
            <div class="form-group col-lg-6 col-md-6 name">
                <input type="text" class="form-control" id="name" placeholder="Enter Name">
            </div>
            <div class="form-group col-lg-6 col-md-6 email">
                <input type="email" class="form-control" id="email" placeholder="Enter email address">
            </div>
        </div>
        <div class="form-group">
            <textarea class="form-control mb-10" rows="5" id="message" placeholder="Menssagem"></textarea>
        </div>
    </form>
    <button class="primary-btn submit_btn" onclick="return storageComment()">Enviar</button>
    <div class="pt-3" id="divAlert"></div>
</div>
<script>
    var globalId = {{$global_Id}};
    var globalType = {{$global_Type}};
</script>
<script>
    var commentType = 0;
    var commentId = 0;
    var old_id = 0;

    function cancelAnswer() {
        window.commentId = windows_old_id;
        window.commentType = 0;
        $('#titleComment').html("Deixe um Comentário");
        $('#buttonCancelAnswer').html("");
    }

    function setGlobalId(id, name) {
        windows_old_id = window.commentId;
        window.commentId = id;
        window.commentType = 1;
        $('#titleComment').html("Responder: " + name);
        $('#buttonCancelAnswer').html("<button class='genric-btn default-border circle small' onclick='cancelAnswer()'>Cancelar<span class='lnr lnr-cross'></span></button>");
    }

    function storageComment() {
        var name = document.getElementById('name');
        var email = document.getElementById('email');
        var message = document.getElementById('message');
        if (name.value == "") {
            $('#divAlert').html("<div class='alert alert-warning' role='alert'>Digite seu nome !</div>");
            name.focus();
        } else {
            if (email.value == "") {
                $('#divAlert').html("<div class='alert alert-warning' role='alert'>Digite seu email !</div>");
                email.focus();
            } else {
                if (message.value == "") {
                    $('#divAlert').html("<div class='alert alert-warning' role='alert'>Escreva o comentário !</div>");
                    message.focus();
                } else {
                    $('#divAlert').html("Enviando.....");
                    var dataString = {
                        'name': name.value,
                        'email': email.value,
                        'message': message.value,
                        'commentType': window.commentType,
                        'commentId': commentId,
                        'type': globalType,
                        'idPost': globalId

                    };
                    var url = window.location.origin + "/cesta/public/recordComment";
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: dataString,
                        beforeSend: function() {
                            $('#divAlert').html("Enviando....");
                        },
                        success: function() {
                            $('#comment-form').html("<div class='alert alert-success' role='alert'>Comentário Enviado !</div>");
                        },
                        error: function() {
                            $('#divAlert').html("<div class='alert alert-danger' role='alert'>Error ao enviar comentário!</div>");
                        }
                    });




                }
            }
        }
    }
</script>