<!--================Header Menu Area =================-->
<header class="header_area">
            <div class="main_menu">
            	<nav class="navbar navbar-expand-lg navbar-light">
					<div class="container">
						<!-- Brand and toggle get grouped for better mobile display -->
						<a class="navbar-brand logo_h" href="{{route('home')}}"><img src="{{asset('img/logo.png')}}" alt=""></a>
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse offset" id="navbarSupportedContent">
							<ul class="nav navbar-nav menu_nav ml-auto">
								<li class="nav-item"><a class="nav-link" href="{{route('home')}}">Início</a></li> 
								<li class="nav-item"><a class="nav-link" href="{{route('about')}}">Sobre</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('allNewsletters')}}">Boletins</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('chartsTables')}}">Tabelas e Gráficos</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('allNews')}}">Notícias</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('academicPublications')}}">Publicações Acadêmicas</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('events')}}">Eventos</a></li>
								<li class="nav-item"><a class="nav-link" href="{{route('contact')}}">Contato</a></li>
							</ul>
						</div> 
					</div>
            	</nav>
            </div>
        </header>
		<!--================Header Menu Area =================-->