<!--================Latest Blog Area =================-->
        <section class="latest_blog_area p_120">
        	<div class="container">
        		<div class="main_title">
        			<h2>Últimas Notícias</h2>
        		</div>
        		<div class="row latest_blog_inner">
					@foreach ($latestNews as $news)
        			<div class="col-lg-3 col-md-6">
        				<div class="l_blog_item">
        					<img class="img-fluid" src="{{asset('img/blog/posts/'. $news->capa)}}" alt="{{$news->titulo}}">
        					<a class="date" href="link">{{date('d/m/Y H:i', strtotime($news->data))}}  |  Por ACCB</a>
        					<a href="{{route('singleNews', ['id' => $news->id])}}"><h4>{{$news->titulo}}</h4></a>
							<p>{{mb_strimwidth($news->corpo, 0, 100, "...")}}</p>
        				</div>
        			</div>
					@endforeach
        		</div>
        	</div>
        </section>
        <!--================End Latest Blog Area =================-->