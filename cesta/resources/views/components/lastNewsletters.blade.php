<!--================Courses Area =================-->
<section class="courses_area p_120">
        	<div class="container">
        		<div class="main_title">
        			<h2>Últimos Boletins</h2>
        		</div>
        		<div class="row courses_inner">
        			<div class="col-lg-12">
						<div class="grid_inner">
							@foreach ($latestNewsletters as $newsletters)
							<div class="grid_item wd44">
								<div class="courses_item">
									<img src="{{asset('img/boletins/boletim.png')}}" alt="">
									<div class="hover_text">
										<a class="cat" href="{{route('singleNewsletters', ['month' => $newsletters->mes, 'year' => $newsletters->ano, 'id' => $newsletters->id])}}">{{$newsletters->mes}} {{$newsletters->ano}}</a> 
										<ul class="list">
											<li><a href="#"><i class="lnr lnr-users"></i> {{$newsletters->visualizacoes}}</a></li>
											<li><a href="{{route('singleNewsletters', ['month' => $newsletters->mes, 'year' => $newsletters->ano, 'id' => $newsletters->id])}}#comments"><i class="lnr lnr-bubble"></i> {{$newsletters->count}}</a></li>
											<li><a href="#"><i class="lnr lnr-user"></i> ACCB</a></li>
										</ul>
									</div>
								</div>
							</div>
							@endforeach
						</div>
        			</div>
        			
        		</div>
        	</div>
        </section>
        <!--================End Courses Area =================-->