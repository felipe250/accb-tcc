<aside class="single_sidebar_widget popular_post_widget">
                                <h3 class="widget_title">Últimas Notícias</h3>
                                @foreach ($latestNews as $news)
                                <div class="media post_item">
                                    <img width="100" height="60" src="{{asset('img/blog/posts/'. $news->capa)}}" alt="{{$news->titulo}}">
                                    <div class="media-body">
                                        <a href="blog-details.html"><h3>{{$news->titulo}}</h3></a>
                                        <p>{{date('d/m/Y', strtotime($news->data))}}</p>
                                    </div>
                                </div>
                                @endforeach
                                <div class="br"></div>
                            </aside>