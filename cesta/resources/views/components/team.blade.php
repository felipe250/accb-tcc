<!--================Team Area =================-->
        <section class="team_area p_120">
        	<div class="container">
        		<div class="main_title">
        			<h2>Equipe</h2>
        			<p>Membros do projeto ACCB.</p>
        		</div>
        		<div class="row team_inner">
					@foreach ($team as $member)
        			<div class="col-lg-3 col-sm-6">
        				<div class="team_item">
        					<div class="team_img">
        						<img class="rounded-circle" src="{{asset('img/team/'.$member->foto)}}" alt="">

        					</div>
        					<div class="team_name">
        						<h4>{{$member->nome}}</h4>
        						<p>{{$member->funcao}}</p>
								<p><a href="{{$member->lattes}}" target="_blank">Lattes</a></p>
        					</div>
        				</div>
        			</div>
					@endforeach        			
        		</div>
        	</div>
        </section>
        <!--================End Team Area =================-->