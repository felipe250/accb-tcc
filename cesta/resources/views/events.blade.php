@extends('layout.accb')

@section('titulo', 'Eventos')

@section('conteudo')

<section class="courses_area p_120">
    <div class="container">
        <div class="main_title">
            <h2>Eventos ACCB/UESC</h2>
        </div>
        <div class="row">
            <div class="col">
                @if($allEvents->isEmpty())
                <div class="alert alert-danger d-flex justify-content-center" role="alert">
                    Nenhum evento cadastrado.
                </div>
                @else
                <div class="row">
                    @foreach($allEvents as $event)
                    <div class="col-md-4 d-flex justify-content-center">
                        <div class="card text-white bg-success mb-3" style="max-width: 18rem;">
                            <div class="card-header">{{$event->titulo}}</div>
                            <div class="card-body">
                                <p class="card-text">{{$event->descricao}}</p>
                                <p>Início: {{date('d/m/Y - H:i', strtotime($event->data_inicio))}} <br>
                                Término: {{date('d/m/Y - H:i', strtotime($event->data_termino))}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endif
            </div>
        </div>
    </div>
</section>
@endsection