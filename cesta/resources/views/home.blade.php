@extends('layout.accb')

@section('titulo', 'Acompanhamento de Custa da Cesta Básica')

@section('conteudo')

        <!--================Home Banner Area =================-->
        <section class="home_banner_area">
            <div class="banner_inner d-flex align-items-center">
                <div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
                <div class="container">
                    <div class="banner_content text-center">
                        <h3>Acompanhamento do Custo da Cesta Básica	</h3>
                        <p>Desde março de 1999, vem sendo desenvolvido o Projeto de Extensão Acompanhamento do Custo da Cesta Básica, pelo Departamento de Ciências Econômicas da Universidade Estadual de Santa Cruz.</p>
                        <a class="main_btn" href="{{route('about')}}">Ler Mais</a>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Home Banner Area =================-->

        <!--================Finance Area =================-->
        <section class="finance_area">
        	<div class="container">
        		<div class="finance_inner row">
        			<div class="col-lg-3 col-sm-6">
        				<div class="finance_item">
        					<div class="media">
        						<div class="d-flex">
        							<i class="lnr lnr-chart-bars"></i>
        						</div>
        						<div class="media-body">
        							<h5>Tabelas & <br />Gráficos</h5>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="finance_item">
        					<div class="media">
        						<div class="d-flex">
        							<i class="lnr lnr-cloud-download"></i>
        						</div>
        						<div class="media-body">
        							<h5>Boletins</h5>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="finance_item">
        					<div class="media">
        						<div class="d-flex">
        							<i class="lnr lnr-book"></i>
        						</div>
        						<div class="media-body">
        							<h5>Notícias</h5>
        						</div>
        					</div>
        				</div>
        			</div>
        			<div class="col-lg-3 col-sm-6">
        				<div class="finance_item">
        					<div class="media">
        						<div class="d-flex">
        							<i class="lnr lnr-graduation-hat"></i>
        						</div>
        						<div class="media-body">
        							<h5>Curso de<br />Economia</h5>
        						</div>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
        <!--================End Finance Area =================-->
		@component('components.lastNewsletters', ['latestNewsletters'=>$latestNewsletters])
		@endcomponent
		@component('components.lastNews', ['latestNews'=>$latestNews])
		@endcomponent
		@component('components.team', ['team'=>$team])
		@endcomponent
@endsection