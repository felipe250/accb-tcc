@extends('layout.accb')

@section('titulo', 'Notícias')

@section('conteudo')
<!--================Blog Area =================-->
<section class="blog_area">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="blog_left_sidebar">
                    @if ($allNews->isEmpty())
                    <div class="alert alert-danger d-flex justify-content-center" role="alert">
                        Nenhuma notícia encontrada.
                    </div>
                    @else
                    @foreach ($allNews as $news)
                    <article class="row blog_item">
                        <div class="col-md-3">
                            <div class="blog_info text-right">
                                <div class="post_tag">
                                    <a href="#">NOME DA CATEGORIA</a>
                                </div>
                                <ul class="blog_meta list">
                                    <li><a href="#">ACCB<i class="lnr lnr-user"></i></a></li>
                                    <li><a href="#">{{date('d/m/Y', strtotime($news->data))}} <i class="lnr lnr-calendar-full"></i></a></li>
                                    <li><a href="#">{{$news->visualizacoes}} Visualizações<i class="lnr lnr-eye"></i></a></li>
                                    <li><a href="{{route('singleNews', ['id' => $news->id])}}#comments">{{$news->count}} Comentários<i class="lnr lnr-bubble"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="blog_post">
                                <!-- <img src="img/blog/main-blog/m-blog-1.jpg" alt=""> -->
                                <img src="{{asset('img/blog/posts/'. $news->capa)}}" alt="{{$news->titulo}}">
                                <div class="blog_details">
                                    <a href="{{route('singleNews', ['id' => $news->id])}}">
                                        <h2>{{$news->titulo}}</h2>
                                    </a>
                                    <p>{{mb_strimwidth($news->corpo, 0, 200, "...")}}</p>
                                    <a href="{{route('singleNews', ['id' => $news->id])}}" class="white_bg_btn">Leia Mais</a>
                                </div>
                            </div>
                        </div>
                    </article>
                    @endforeach
                    {{$allNews->links()}}
                    @endif
                </div>
            </div>
            <div class="col-lg-4">
                <div class="blog_right_sidebar">
                    <aside class="single_sidebar_widget search_widget">
                        <form action="" method="POST">
                            @csrf
                            <div class="input-group">
                                <input type="text" name="search" class="form-control" placeholder="Buscar Notícias">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="lnr lnr-magnifier"></i></button>
                                </span>

                            </div><!-- /input-group -->
                        </form>
                        <div class="br"></div>
                    </aside>
                    @component('components.popularNews', ['popularNews'=>$popularNews])
                    @endcomponent
                    @component('components.categories', ['allCategories'=>$allCategories])
                    @endcomponent
                </div>
            </div>
        </div>
    </div>
</section>
<!--================Blog Area =================-->
@endsection