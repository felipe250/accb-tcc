@extends('layout.accb')

@section('titulo', 'Boletins Mensais ACCB/UESC')

@section('conteudo')
<!--================Courses Area =================-->
<section class="courses_area p_120">
	<div class="container">
		<div class="main_title">
			<h2>Boletins Mensais ACCB/UESC</h2>
		</div>
		<div class="col-12">
			<div class="row d-flex justify-content-center">
				<h4>Apresentação</h4>
			</div>
			<p><smal>O boletim ACCB/UESC é uma produção do Projeto Acompanhamento do Custo da Cesta Básica
			 do Departamento de Ciências Econômicas da Universidade Estadual de Santa Cruz. 
Os boletins contêm informações acerca das variações de preço dos produtos que compõem a cesta básica oficial
 a partir de levantamento de preço realizado em estabelecimentos comerciais das cidades baianas de Ilhéus
  e Itabuna. Aqui são disponibilizadas informações acerca do gasto mensal, preço médio, tempo de trabalho 
  necessário, variação mensal, semestral, anual e do ano de cada item da cesta. Também são feitas análises 
  conjunturais sobre os principais fatores que geram os movimentos dos preços dos produtos da cesta básica.
   Essa publicação é encaminhada mensalmente para os meios de comunicação impresso, televisivo e eletrônico.

			</smal></p>
		</div>
		<div class="row">
			<div class="col-lg-8"><!-- Start Newsletters -->
				<div class="row d-flex justify-content-center col-lg-12">
					<div class="col-2">
						<div class="single-element-widget">
							<div class="default-select" id="default-select">
								<select id="selectYear" onchange="showNewslettersByYear()">
									@foreach ($allYears as $year)
									@if ($loop->first)
										@if($year->ano == now()->year)
											<option selected value="{{$year->ano}}">{{$year->ano}}</option>
										@else
											<option selected value="{{now()->year}}">{{now()->year}}</option>
											<option value="{{$year->ano}}">{{$year->ano}}</option>
										@endif
									@else
									<option value="{{$year->ano}}">{{$year->ano}}</option>
									@endif

									@endforeach
								</select>
							</div>
						</div>
					</div>
				</div>


				<div class="row courses_inner">
					<div id="newsletters" class="grid_inner col-lg-12">
						@if ($allNewsletters->isEmpty())
						<div class="alert alert-warning text-center" role="alert">Nenhum boletim encontrado para esse ano !</div>
						@else
						@foreach ($allNewsletters as $newsletters)
						<div class="grid_item wd44">
							<div class="courses_item">
								<img width="100%" height="100%" src="{{asset('img/boletins/boletim.png')}}" alt="Boletim"/>
								<div class="hover_text">
									<a class="cat" href="{{route('singleNewsletters', ['month' => $newsletters->mes, 'year' => $newsletters->ano, 'id' => $newsletters->id])}}">{{$newsletters->mes}} {{$newsletters->ano}}</a>
									<ul class="list">
										<li><a href="#"><i class="lnr lnr-users"></i> {{$newsletters->visualizacoes}}</a></li>
										<li><a href="{{route('singleNewsletters', ['month' => $newsletters->mes, 'year' => $newsletters->ano, 'id' => $newsletters->id])}}#comments"><i class="lnr lnr-bubble"></i>  {{$newsletters->count}}</a></li>
										<li><a href="#"><i class="lnr lnr-user"></i> ACCB</a></li>
									</ul>
								</div>
							</div>
						</div>
						@endforeach
						@endif
					</div>
				</div>
			</div><!-- End Newsletters-->
			<div class="col-lg-4">
				<div class="row d-flex justify-content-center">
					<h4>Dados da publicação</h4>
				</div>
				<ul class="list-unstyled">
					<br>
					<li><h6>Corpo Editorial</h6>
						<ul>
						<li>Dany Sanchez Dominguez (UESC/DCET)</li>
						<li>Gustavo Joaquim Lisboa (UESC/DCEC)</li>
						<li>Marcelo Inácio Ferreira Ferraz (UESC/DCET)</li>
						<li>Mônica de Moura Pires (UESC/DCEC)</li>
						<li>Paulo César Cruz Dantas (UESC)</li>
						</ul>
					</li>
					<br>
					<li><h6>Autor corporativo</h6>
						<ul>
							<li>Laboratório de Análises Econômicas Regionais (LABOR)</li>
							<li>Departamento de Ciências Econômicas (DCEC)</li>
							<li>Universidade Estadual de Santa Cruz (UESC)</li>
						</ul>
					</li>
					<br>
					<li><h6>Endereço</h6>
						<ul>
							<li>Universidade Estadual de Santa Cruz - Departamento de Ciências Econômicas</li>
							<li>Rodovia Jorge Amado, Km 16 - Salobrinho Ilhéus- Bahia - Brasil CEP 45662-900</li>
							<li>Fone: +55(73)3680-5215/5238</li>
							<li>e-mails: cestabasica@uesc.br e cbuesc@gmail.com</li>
						</ul>
					</li>
					<br>
					<li><h6>Formato</h6>
						<ul>
							<li>Eletrônico</li>
						</ul>
					</li>
					<br>
					<li><h6>Periodicidade da publicação</h6>
						<ul>
							<li>Mensal</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div> <!--End Container -->
</section>

					<script>
						function showNewslettersByYear(){
							var yearSelected = document.getElementById("selectYear").value;
							var url = 'boletins/getNewslettersByYear/' + yearSelected;
							$.ajax({
								async: true,
								type: "GET",
								url: url,
								success: function(data) {
									console.log(data);
									var html = "";
								if(data.length){
									data.forEach(function(element, index, array){

									html += "<div class='grid_item wd44'> <div class='courses_item'>";
									html += "<img width='100%' height='100%'src='{{asset('img/boletins/boletim.png')}}' alt='Boletim'> <div class='hover_text'>";
									html += "<a class='cat' href='boletins/"+ element['mes'] +"/"+ element['ano'] +"/"+ element['id']+" '>"+element['mes']+" "+element['ano']+"</a>";
									html += "<ul class='list'><li><a href='#'><i class='lnr lnr-users'></i> "+element['visualizacoes']+"</a></li>";
									html += "<li><a href='boletins/"+ element['mes'] +"/"+ element['ano'] +"/"+ element['id']+"#comments '><i class='lnr lnr-bubble'></i> "+element['count']+"</a></li>";
									html += "<li><a href='#'><i class='lnr lnr-user'></i> ACCB</a></li>";
									html += "</ul></div></div></div>";

									});
								}else{
									html += "<div class='alert alert-warning text-center' role='alert'>Nenhum boletim encontrado para esse ano !</div>";
								}
								$("#newsletters").html(html);
								},
								error: function() {
									alert("Não foi possível carregar os boletins!");
								}
							});
						}
					</script>
@endsection