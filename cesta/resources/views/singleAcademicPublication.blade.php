@extends('layout.accb')

@section('titulo', $publication->titulo)

@section('conteudo')
<!--================Course Details Area =================-->
<section class="course_details_area p_120">
	<div class="container">
		<div class="row course_details_inner">
			<div class="col-lg-8">
				<div class="row">
					<iframe src="http://nbcgib.uesc.br/colcic/document/comp_curriculares/matriz_cic_2012.pdf" width="100%" height="500px" style="border: none;"></iframe>
				</div>
				<ul class="nav nav-tabs" id="myTab" role="tablist">
					<li class="nav-item">
						<a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Informações</a>
					</li>
					<li class="nav-item">
						<a class="nav-link active" id="comments-tab" data-toggle="tab" href="#comments" role="tab" aria-controls="comments" aria-selected="false">Comentários</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
						<div class="objctive_text">
							<p>{{$publication->informacoes}}</p>
						</div>
					</div>
					<div class="tab-pane fade show active" id="comments" role="tabpanel" aria-labelledby="comments-tab">
					@component('components.comments', ['allComments'=>$allComments, 'global_Id'=>$publication->id, 'global_Type'=>2])
						@endcomponent
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="c_details_list">
					<ul class="list">
						<li><a href="#">Veículo<span>{{$publication->veiculo}}</span></a></li>
						<li><a href="#">Tipo <span>{{$publication->tipo}}</span></a></li>
						<li><a href="#">Data<span>{{date('d/m/Y', strtotime($publication->data))}}</span></a></li>
						<li><a href="#">Autores <span>ACCB</span></a></li>
					</ul>
					<a class="main_btn" href="{{asset('upload/publicacoes/'. $publication->publicacao)}}" target="_blank">Baixar PDF </a>
				</div>
			</div>
		</div>
	</div>
</section>
<!--================End Course Details Area =================-->
@endsection