@extends('layout.accb')

@section('titulo', $news->titulo)

@section('conteudo')
<!--================Blog Area =================-->
<section class="blog_area single-post-area p_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 posts-list">
                        <div class="single-post row">
                            <div class="col-lg-12">
                                <div class="feature-img">
                                    <img class="img-fluid" src="{{asset('img/blog/posts/'. $news->capa)}}" alt="{{$news->titulo}}">
                                </div>									
                            </div>
                            <div class="col-lg-3  col-md-3">
                                <div class="blog_info text-right">
                                    <div class="post_tag">
                                        <a href="#">CATEGORIA</a>
                                    </div>
                                    <ul class="blog_meta list">
                                        <li><a href="#">Autor<i class="lnr lnr-user"></i></a></li>
                                        <li><a href="#">{{$news->data}}<i class="lnr lnr-calendar-full"></i></a></li>
                                        <li><a href="#">{{$news->visualizacoes}} Visualizações<i class="lnr lnr-eye"></i></a></li>
                                        <li><a href="#">06 Comentários<i class="lnr lnr-bubble"></i></a></li>
                                    </ul>
                                    <ul class="social-links">
                                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                        <li><a href="#"><i class="fa fa-github"></i></a></li>
                                        <li><a href="#"><i class="fa fa-behance"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 blog_details">
                                <h2>{{$news->titulo}}</h2>
                                <p class="excert">
                                    {{$news->corpo}}
                                </p>
                            </div>
                        </div>
                        
                        @component('components.comments', ['allComments'=>$allComments, 'global_Id'=>$news->id, 'global_Type'=>1])
                        @endcomponent
                    </div>
                    <div class="col-lg-4">
                        <div class="blog_right_sidebar">
                            @component('components.latestNews', ['latestNews'=>$latestNews])
                            @endcomponent
                            @component('components.categories', ['allCategories'=>$allCategories])
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================Blog Area =================-->
@endsection