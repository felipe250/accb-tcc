<?php

Route::get('/', 'Home@showHome')->name('home');
Route::get('graficosetabelas', function (){
                    return view('chartsTables');
                })->name('chartsTables');

Route::get('graficosetabelas/getData', 'TablesCharts@getData');

route::get('sobre', 'About@showAbout')->name('about');

Route::prefix('contato')->group(function (){
    route::get('/', function (){
        return view('contact');
    })->name('contact');
    Route::post('/', 'Contact@storeMessage');
});
Route::prefix('noticias')->group(function (){
    Route::get("/", 'News@showAll')->name('allNews');
    Route::get("{id}", 'News@getNewsById')->name('singleNews');
    Route::get("categoria/{id}", 'News@getAllNewsByCategorie')->name('getAllNewsByCategorie');
    Route::post('/', 'News@searchNews');
});

Route::prefix('publicacoes')->group(function(){
    Route::get("/", 'academicPublications@showAll')->name('academicPublications');
    Route::get("{id}", 'academicPublications@getPublicationById')->name('singleAcademicPublication');
    Route::post('/', 'academicPublications@searchPublications');
});

Route::prefix('eventos')->group(function(){
    Route::get("/", 'Events@showAll')->name('events');
});

Route::prefix('boletins')->group(function (){
    Route::get("/", 'Newsletters@showAll')->name('allNewsletters');
    /*Route::get("/", function(){
        return view('newsletters');
    })->name('allNewsletters');*/
    Route::get("{month}/{year}/{id}", 'Newsletters@getNewslettersById')->name('singleNewsletters');
    Route::get("getNewslettersByYear/{year}", 'Newsletters@getNewslettersByYear')->name('getNewsletterByYear');
});

Route::post('recordComment', 'Comments@recordComment');
